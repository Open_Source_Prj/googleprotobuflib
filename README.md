# GoogleProtobufLib

Data serialization using Google Protobuf.
https://developers.google.com/protocol-buffers/
https://developers.google.com/protocol-buffers/docs/csharptutorial


Google Protobuf library with proto compiler.

Example of how to use and integrate Google Protobuf for a C# project.

Get Google Protobuf protoc.exe compiler (Example for Google.Protobuf.Tools.3.7.0):
1. Create a new C# Console Application
2. Navigate to Manage NuGet Packages
3. Search and install Google.Protobuf.Tools
4. From solution directory navigate to packages directory 
5. Google.Protobuf.Tools -> tools -> windows_x64
6. Get protoc.exe compiler and use it

Protoc.exe usage (Run from CMD): protoc -I=$SRC_DIR --csharp_out=$DST_DIR $SRC_DIR/example.proto


To use Google Protobuf library in a C# project install Google.Protobuf from NuGet Manager. 

Example of Protobuf Message:

syntax = "proto3";

message Phonebook
{
    message Person
    {
        string      Name        = 1;
        string      PhoneNumber = 2;
    }

    repeated Person     Persons = 1;
}

To compile the .proto file, open CMD and navigate to "Compiler" directory and run "compile.bat example.proto".
This command will generate a C# class ready to be used in a project.