﻿using Google.Protobuf;
using Newtonsoft.Json;
using System;
using System.IO;

namespace ProtoExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create entries and save to file
            UserSaves saves = new UserSaves();

            saves.Add("Ana", "07564545545");
            saves.Add("Bica", "1919191919");
            saves.Add("Petru", "3030292929");

            saves.SaveBinaryToFile();

            //Another object used to load from files
            UserSaves temp = new UserSaves();

            temp.LoadFromProtoBinary();
            temp.PrintOnScreen();
            temp.SaveAsJson();
            temp.LoadAsJson();
        }
    }

    class UserSaves
    {
        private const string fileNameProtoText = "ProtoText.dat";
        private const string fileNameJsonIndented = "JsonIndented.dat";
        private const string fileNameProtoBinary = "ProtoBinary.dat";
        private Phonebook _phonebook;


        public UserSaves()
        {
            _phonebook = new Phonebook();
        }

        public void Add(string name, string phoneNumber)
        {
            Phonebook.Types.Person temp = new Phonebook.Types.Person
            {
                Name = name,
                PhoneNumber = phoneNumber
            };

            _phonebook.Persons.Add(temp);
        }

        public void LoadFromProtoBinary()
        {
            using (var input = File.OpenRead(fileNameProtoBinary))
            {
                _phonebook = Phonebook.Parser.ParseFrom(input);
            }
        }

        public void PrintOnScreen()
        {
            foreach(var v in _phonebook.Persons)
            {
                string msg = string.Format("Name: {0} Phone: {1}", v.Name, v.PhoneNumber);

                Console.WriteLine(msg);
            }
        }

        public void SaveBinaryToFile()
        {
            using (var output = File.Create(fileNameProtoBinary))
            {
                _phonebook.WriteTo(output);
            }
        }

        public void LoadAsJson()
        {
            string text = File.ReadAllText(fileNameProtoText);

            _phonebook = Phonebook.Parser.ParseJson(text);
        }

        public void SaveAsJson()
        {
            SaveJsonIndented();
            SaveJsonAsString();
        }

        private void SaveJsonIndented()
        {
            string indent = JsonConvert.SerializeObject(_phonebook, Formatting.Indented);

            File.WriteAllText(fileNameJsonIndented, indent);
        }

        private void SaveJsonAsString()
        {
            File.WriteAllText(fileNameProtoText, _phonebook.ToString());
        }
    }
}
